import QtQuick 2.0
import QtWebEngine 1.5
import QtQuick.Layouts 1.1
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.core 2.0 as PlasmaCore

Item {
    id: root

    width: 1300
    height: 1090
    Layout.minimumWidth :  1024
    visible: true

    WebEngineView {
        anchors.fill: parent
        url: "https://pos.esss.lu.se"
    }
}

