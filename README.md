# ESS Status Page Widget for KDE 5

## Install

```
kpackagetool5 -t Plasma/Applet --install essstatus
```

## Upgrade

```
kpackagetool5 -t Plasma/Applet --upgrade essstatus
```

## Uninstall

```
kpackagetool5 -t Plasma/Applet --remove  essstatus
```
